<?php

namespace modules;

use \lib\core\Module;

require_once __DIR__ . '/../../../vendor/autoload.php';


class HtmlToPDF extends Module
{
    public function ConvertToPDF($options, $name)
    {
        
        $pageHeight = $this->app->parseObject($options->pageHeight);
        $pageWidth = $this->app->parseObject($options->pageWidth);
        $fileName = $this->app->parseObject($options->fileName);
        $folderName = $this->app->parseObject($options->folderName);
        
        // temp folder path, create if not exist
        $tempFolder = dirname(__FILE__) . '/../../../' . $this->app->parseObject($options->tempFolder);
        if (!file_exists($tempFolder)) {
            mkdir($tempFolder, 0777, true);
        }

        // output pdf folder path, create if not exist
        $outputDir = dirname(__FILE__) . '/../../../' . $this->app->parseObject($options->folderName);
        if (!file_exists($outputDir)) {
            mkdir($outputDir, 0777, true);
        }
        
        // if margin left is blank then margin left 0
        $options->bodyMarginLeft = property_exists($options, 'bodyMarginLeft') ? is_null($options->bodyMarginLeft) ? 0 : $this->app->parseObject($options->bodyMarginLeft) : 0;

         // if margin top is blank then margin top 0
        $options->bodyMarginTop = property_exists($options, 'bodyMarginTop') ? is_null($options->bodyMarginTop) ? 0 : $this->app->parseObject($options->bodyMarginTop) : 0;

         // if margin right is blank then margin right 0
        $options->bodyMarginRight = property_exists($options, 'bodyMarginRight') ? is_null($options->bodyMarginRight) ? 0 : $this->app->parseObject($options->bodyMarginRight) : 0;

         // if margin bottom is blank then margin bottom 0
        $options->bodyMarginBottom = property_exists($options, 'bodyMarginBottom') ? is_null($options->bodyMarginBottom) ? 0 : $this->app->parseObject($options->bodyMarginBottom) : 0;

        // if header/footer html is empty then setting it as null
        $options->headerHTML = property_exists($options, 'headerHTML') ? $this->app->parseObject($options->headerHTML) : null;
        $options->footerHTML = property_exists($options, 'footerHTML') ? $this->app->parseObject($options->footerHTML) : null;

        // if header/footer margin is empty then setting header margin as 0 mm 
        $options->headerMargin = property_exists($options, 'headerMargin') ? is_null($options->headerHTML) ? 0 : $this->app->parseObject($options->headerMargin) : 0;
        $options->footerMargin = property_exists($options, 'footerMargin') ? is_null($options->footerHTML) ? 0 : $this->app->parseObject($options->footerMargin) : 0;
    
        $mPDFOptions = [
            'mode' => 'utf-8',
            'orientation' => $options->orientation, // page orientation portrait/landscape
            "margin_left" => $options->bodyMarginLeft, // page body margin left
            "margin_top" => $options->bodyMarginTop, // page body margin top
            "margin_right" => $options->bodyMarginRight, // page body margin right
            "margin_bottom" => $options->bodyMarginBottom, // page body margin bottom
            "margin_header" => $options->headerMargin, // page header margin
            "margin_footer" => $options->footerMargin, // page footer margin 
            "tempDir" => $tempFolder, // temparary folder path used by mpdf for storing temp fonts and cache things. 
        ];
        
        // if paper size comes empty then setting page size as A4 
        $paperSize = property_exists($options, 'paperSize') ? (is_null($options->paperSize) ? 'A4' : $options->paperSize) : 'A4';
        if($paperSize == 'Custom') { // if paper size is custom then setting custom height and width
            $mPDFOptions['format'] = [$pageWidth, $pageHeight];
        }
        else {
            $mPDFOptions['format'] = $paperSize;
        }
        
        // generate final pdf with all above mentioned option
        $mpdf = new \Mpdf\Mpdf($mPDFOptions);
        $mpdf->showImageErrors = true;
        $mpdf->simpleTables = true;

        // if header html is not empty then setting header template
        if (!is_null($options->headerHTML)) {
            $mpdf->SetHTMLHeader($options->headerHTML);
        }

        // if footer html is not empty then setting footer template
        if (!is_null($options->footerHTML)) {
            $mpdf->SetHTMLFooter($options->footerHTML);
        }
    
        // setting the main body html
        $html = $this->app->parseObject($options->bodyHTML);

        // Add a page
        $mpdf->AddPage();
        $mpdf->WriteHTML($html); // writing html to a page
        
        // output pdf file name
        $filePath = $outputDir . '/' . $fileName . '.pdf';
        
        $mpdf->Output($filePath, 'F');
        
        $returnObject = new \stdClass();
        $returnObject->FileName = $fileName . '.pdf';
        $returnObject->FilePath = $folderName .'/'.$fileName . '.pdf';
        //$returnObject->FolderPath = $outputDir;
        
        return $returnObject;
    }
}