<!doctype html>
<html>

<head>
    <meta name="ac:route" content="/">
    <base href="/">
    <script src="dmxAppConnect/dmxAppConnect.js"></script>
    <meta charset="UTF-8">
    <title>Wappler Extensions - HTML to PDF</title>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="bootstrap/4/css/bootstrap.min.css" />
    <script src="js/moment.js/2/moment.min.js"></script>
    <script src="dmxAppConnect/dmxBootstrap4Navigation/dmxBootstrap4Navigation.js" defer=""></script>
    <script src="dmxAppConnect/dmxFormatter/dmxFormatter.js" defer=""></script>
    <script src="dmxAppConnect/dmxDownload/dmxDownload.js" defer=""></script>
    <link rel="stylesheet" href="dmxAppConnect/dmxDatePicker/daterangepicker.min.css" />
    <script src="dmxAppConnect/dmxDatePicker/daterangepicker.min.js" defer=""></script>
    <link rel="stylesheet" href="dmxAppConnect/dmxDatePicker/dmxDatePicker.css" />
    <script src="dmxAppConnect/dmxDatePicker/dmxDatePicker.js" defer=""></script>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.14.0/css/all.css" integrity="sha384-HzLeBuhoNPvSl5KYnjx0BT+WB0QEEqLprO+NBkkk5gbc67FTaL7XIGa2w1L0Xbgc" crossorigin="anonymous" />
</head>

<body is="dmx-app" id="index">
    <dmx-download id="downloadFile" dmx-bind:url="varFilePath.value"></dmx-download>
    <dmx-value id="varFilePath"></dmx-value>
    <div class="container-fluid">
        <div class="row">
            <div class="col">
                <nav class="justify-content-center justify-content-md-between navbar navbar-expand-lg px-0">
                    <a class="d-flex align-items-center navbar-brand text-body" href="https://community.wappler.io/t/about-the-wappler-extensions-category/26436" target="_blank" ref="noopener">
                        <img src="https://community.wappler.io/uploads/default/original/1X/3b802060c30167b96f94ef35fbd0bdf3328d7e3b.png" height="36" class="mr-3">Extensions - HTML to PDF</a>
                    <a class="navbar-brand" href="http://slashash.co" target="_blank" title="Designed &amp; Developed by Slashash Tech LLP" ref="noopener">
                        <img src="https://slashash.co/i/made_by_SLASHASH.svg" alt="Designed &amp; Developed by Slashash Tech LLP"></a>
                </nav>
            </div>
        </div>
    </div>
    <div class="container-fluid pb-5 pt-mb-5 pt-2 wappler-block">
        <ul class="nav nav-tabs" id="myTab" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" id="about-tab" data-toggle="tab" href="#aboutTab" role="tab" aria-controls="home" aria-selected="true">About</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="withHeader-tab" data-toggle="tab" href="#withHeader" role="tab" aria-controls="home" aria-selected="true">Generate PDF</a>
            </li>
        </ul>
        <div class="tab-content" id="myTabContent">
            <div class="tab-pane fade show active p-3" id="aboutTab" role="tabpanel" aria-labelledby="about-tab">
                <h4><i class="far fa-file-pdf fa-lg mr-2"></i>HTML to PDF</h4>
                <p>This custom action allows you to create PDF file from custom HTML. This extension adds to the already great set of components Wappler provided by default. This module is available for both PHP and NodeJS server models.</p>
                <p>Add HTML to PDF step in server connect from File Management category, specify an output folder path & name along with your custom HTML and the module will create a PDF from that HTML and save it.<br />Your custom HTML can contain
                    images via URL or absolute system path for local images.<br />You can then use that PDF to send as attachment via email or just store the path in DB.</p>
                <p>Almost all options are explained using <i class="border py-1 px-2 border-dark fas fa-info"></i> help texts. Please make sure to read them.</p>
                <h4 class="mt-4">Known Issues</h4>
                <ul>
                    <li><a href="https://mpdf.github.io/" target="_blank" rel="noopener">mPDF</a>, the library used for HTML to PDF generation, has various limitations and known issues which are also applicable here.</li>
                    <li>Few input fields in step might behave inappropriately. This is a <a href="https://community.wappler.io/t/custom-module-conditional-items-show-up-incorrectly/29550" target="_blank" rel="noopener">reported</a> Wappler bug.
                        Please be
                        vigilant when configuring the module until this is fixed by Wappler team.</li>
                </ul>
                <h4 class="mt-4">Download</h4>
                <div class="d-block mb-2">Main custom module files are stored in <mark>extensions/server_connect/modules</mark>.</div>
                <div class="d-block mb-2">Package Dependencies: <a target="_blank" rel="noopener" href="https://packagist.org/packages/mpdf/mpdf">mPDF/mPDF</a></div>
                <span>
                    NodeJS:
                    <a title="Git Lab" class="border rounded p-2 ml-2" rel="noopener" href="https://gitlab.com/wappler-extensions/html-to-pdf-nodejs" target="_blank">
                        <svg width="24" height="24" class="tanuki-logo" viewBox="0 0 36 36">
                            <path class="tanuki-shape tanuki-left-ear" fill="#e24329" d="M2 14l9.38 9v-9l-4-12.28c-.205-.632-1.176-.632-1.38 0z"></path>
                            <path class="tanuki-shape tanuki-right-ear" fill="#e24329" d="M34 14l-9.38 9v-9l4-12.28c.205-.632 1.176-.632 1.38 0z"></path>
                            <path class="tanuki-shape tanuki-nose" fill="#e24329" d="M18,34.38 3,14 33,14 Z"></path>
                            <path class="tanuki-shape tanuki-left-eye" fill="#fc6d26" d="M18,34.38 11.38,14 2,14 6,25Z"></path>
                            <path class="tanuki-shape tanuki-right-eye" fill="#fc6d26" d="M18,34.38 24.62,14 34,14 30,25Z"></path>
                            <path class="tanuki-shape tanuki-left-cheek" fill="#fca326" d="M2 14L.1 20.16c-.18.565 0 1.2.5 1.56l17.42 12.66z"></path>
                            <path class="tanuki-shape tanuki-right-cheek" fill="#fca326" d="M34 14l1.9 6.16c.18.565 0 1.2-.5 1.56L18 34.38z"></path>
                        </svg>
                        <svg width="42" height="42" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 617 169">
                            <path
                                d="M315.26 2.97h-21.8l.1 162.5h88.3v-20.1h-66.5l-.1-142.4M465.89 136.95c-5.5 5.7-14.6 11.4-27 11.4-16.6 0-23.3-8.2-23.3-18.9 0-16.1 11.2-23.8 35-23.8 4.5 0 11.7.5 15.4 1.2v30.1h-.1m-22.6-98.5c-17.6 0-33.8 6.2-46.4 16.7l7.7 13.4c8.9-5.2 19.8-10.4 35.5-10.4 17.9 0 25.8 9.2 25.8 24.6v7.9c-3.5-.7-10.7-1.2-15.1-1.2-38.2 0-57.6 13.4-57.6 41.4 0 25.1 15.4 37.7 38.7 37.7 15.7 0 30.8-7.2 36-18.9l4 15.9h15.4v-83.2c-.1-26.3-11.5-43.9-44-43.9M557.63 149.1c-8.2 0-15.4-1-20.8-3.5V70.5c7.4-6.2 16.6-10.7 28.3-10.7 21.1 0 29.2 14.9 29.2 39 0 34.2-13.1 50.3-36.7 50.3m9.2-110.6c-19.5 0-30 13.3-30 13.3v-21l-.1-27.8h-21.3l.1 158.5c10.7 4.5 25.3 6.9 41.2 6.9 40.7 0 60.3-26 60.3-70.9-.1-35.5-18.2-59-50.2-59M77.9 20.6c19.3 0 31.8 6.4 39.9 12.9l9.4-16.3C114.5 6 97.3 0 78.9 0 32.5 0 0 28.3 0 85.4c0 59.8 35.1 83.1 75.2 83.1 20.1 0 37.2-4.7 48.4-9.4l-.5-63.9V75.1H63.6v20.1h38l.5 48.5c-5 2.5-13.6 4.5-25.3 4.5-32.2 0-53.8-20.3-53.8-63-.1-43.5 22.2-64.6 54.9-64.6M231.43 2.95h-21.3l.1 27.3v94.3c0 26.3 11.4 43.9 43.9 43.9 4.5 0 8.9-.4 13.1-1.2v-19.1c-3.1.5-6.4.7-9.9.7-17.9 0-25.8-9.2-25.8-24.6v-65h35.7v-17.8h-35.7l-.1-38.5M155.96 165.47h21.3v-124h-21.3v124M155.96 24.37h21.3V3.07h-21.3v21.3">
                            </path>
                        </svg>
                    </a>
                </span>
                <span class="mx-2 font-weight-bold lead">|</span>
                <span>
                    PHP:
                    <a title="Git Lab" class="border rounded p-2 ml-2" rel="noopener" href="https://gitlab.com/wappler-extensions/html-to-pdf-php" target="_blank">
                        <svg width="24" height="24" class="tanuki-logo" viewBox="0 0 36 36">
                            <path class="tanuki-shape tanuki-left-ear" fill="#e24329" d="M2 14l9.38 9v-9l-4-12.28c-.205-.632-1.176-.632-1.38 0z"></path>
                            <path class="tanuki-shape tanuki-right-ear" fill="#e24329" d="M34 14l-9.38 9v-9l4-12.28c.205-.632 1.176-.632 1.38 0z"></path>
                            <path class="tanuki-shape tanuki-nose" fill="#e24329" d="M18,34.38 3,14 33,14 Z"></path>
                            <path class="tanuki-shape tanuki-left-eye" fill="#fc6d26" d="M18,34.38 11.38,14 2,14 6,25Z"></path>
                            <path class="tanuki-shape tanuki-right-eye" fill="#fc6d26" d="M18,34.38 24.62,14 34,14 30,25Z"></path>
                            <path class="tanuki-shape tanuki-left-cheek" fill="#fca326" d="M2 14L.1 20.16c-.18.565 0 1.2.5 1.56l17.42 12.66z"></path>
                            <path class="tanuki-shape tanuki-right-cheek" fill="#fca326" d="M34 14l1.9 6.16c.18.565 0 1.2-.5 1.56L18 34.38z"></path>
                        </svg>
                        <svg width="42" height="42" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 617 169">
                            <path
                                d="M315.26 2.97h-21.8l.1 162.5h88.3v-20.1h-66.5l-.1-142.4M465.89 136.95c-5.5 5.7-14.6 11.4-27 11.4-16.6 0-23.3-8.2-23.3-18.9 0-16.1 11.2-23.8 35-23.8 4.5 0 11.7.5 15.4 1.2v30.1h-.1m-22.6-98.5c-17.6 0-33.8 6.2-46.4 16.7l7.7 13.4c8.9-5.2 19.8-10.4 35.5-10.4 17.9 0 25.8 9.2 25.8 24.6v7.9c-3.5-.7-10.7-1.2-15.1-1.2-38.2 0-57.6 13.4-57.6 41.4 0 25.1 15.4 37.7 38.7 37.7 15.7 0 30.8-7.2 36-18.9l4 15.9h15.4v-83.2c-.1-26.3-11.5-43.9-44-43.9M557.63 149.1c-8.2 0-15.4-1-20.8-3.5V70.5c7.4-6.2 16.6-10.7 28.3-10.7 21.1 0 29.2 14.9 29.2 39 0 34.2-13.1 50.3-36.7 50.3m9.2-110.6c-19.5 0-30 13.3-30 13.3v-21l-.1-27.8h-21.3l.1 158.5c10.7 4.5 25.3 6.9 41.2 6.9 40.7 0 60.3-26 60.3-70.9-.1-35.5-18.2-59-50.2-59M77.9 20.6c19.3 0 31.8 6.4 39.9 12.9l9.4-16.3C114.5 6 97.3 0 78.9 0 32.5 0 0 28.3 0 85.4c0 59.8 35.1 83.1 75.2 83.1 20.1 0 37.2-4.7 48.4-9.4l-.5-63.9V75.1H63.6v20.1h38l.5 48.5c-5 2.5-13.6 4.5-25.3 4.5-32.2 0-53.8-20.3-53.8-63-.1-43.5 22.2-64.6 54.9-64.6M231.43 2.95h-21.3l.1 27.3v94.3c0 26.3 11.4 43.9 43.9 43.9 4.5 0 8.9-.4 13.1-1.2v-19.1c-3.1.5-6.4.7-9.9.7-17.9 0-25.8-9.2-25.8-24.6v-65h35.7v-17.8h-35.7l-.1-38.5M155.96 165.47h21.3v-124h-21.3v124M155.96 24.37h21.3V3.07h-21.3v21.3">
                            </path>
                        </svg>
                    </a>
                </span>
            </div>
            <div class="tab-pane fade p-3" id="withHeader" role="tabpanel" aria-labelledby="withHeader-tab">
                <h4>HTML to PDF Using mPDF</h4>
                <p>Server Actions: <mark>GeneratePDF</mark></p>
                <div class="row">
                    <div class="col-md-6 co-12">
                        <form action="dmxConnect/api/GeneratePDF.php" method="post" is="dmx-serverconnect-form" id="formWithHeaderFooter" site="HtmlToPDF_PHP" dmx-on:success="varFilePath.setValue(formWithHeaderFooter.data.FilePath)">
                            <h6 class="mt-2">REQUEST:</h6>
                            <div class="form-group row">
                                <div class="col-md-5 col-12">
                                    <label>Header &amp; Footer?</label>
                                </div>
                                <div class="col-md-6 col-12">
                                    <select class="form-control" name="HeaderFooter">
                                        <option value="1" selected>Yes</option>
                                        <option value="0">No</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-5 col-12">
                                    <label>Your Full Name</label>
                                </div>
                                <div class="col-md-6 col-12">
                                    <input type="text" class="form-control" required name="FullName" placeholder="Full Name">
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-5 col-12">
                                    <label>Any Date</label>
                                </div>
                                <div class="col-md-6 col-12">
                                    <input type="text" class="form-control" name="Date" required is="dmx-date-picker" format="YYYY-MM-DD">
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-5 col-12">
                                    <label>File Name (without extension)</label>
                                </div>
                                <div class="col-md-6 col-12">
                                    <input type="text" class="form-control" required name="FileName" placeholder="example">
                                </div>
                            </div>
                            <button type="submit" class="btn btn-success" dmx-bind:disabled="state.executing">Submit<span class="spinner-border spinner-border-sm ml-2" role="status" dmx-show="state.executing"></span>
                            </button>
                        </form>
                    </div>
                    <div class="col-md-6 col-12">
                        <h6 class="mt-4 mt-md-2">RESPONSE:</h6>
                        <button dmx-show="varFilePath.value && formWithHeaderFooter.status == 200" class="btn btn-info mb-2" dmx-on:click="downloadFile.download()">Download PDF<span dmx-show="DownloadPDF.state.executing"
                                class="ml-1 spinner-border spinner-border-sm" role="status"></span></button>
                        <pre style="height:120px;" dmx-class:text-danger="formWithHeaderFooter.status != 200"
                            class="bg-light p-2 border rounded-lg">{{formWithHeaderFooter.status != 200 ? formWithHeaderFooter.lastError.response.message : formWithHeaderFooter.data.stringifyJSON()}}</pre>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="bootstrap/4/js/popper.min.js"></script>
    <script src="bootstrap/4/js/bootstrap.min.js"></script>
    <script>
        $(function () {
            dmx.Formatter('object', 'stringifyJSON', function (param) {
                return JSON.stringify(param, null, 2);
            }
            );
        });
    </script>
</body>

</html>