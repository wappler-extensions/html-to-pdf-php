<?php
require('../../dmxConnectLib/dmxConnect.php');


$app = new \lib\App();

$app->define(<<<'JSON'
{
  "meta": {
    "options": {
      "linkedFile": "/index.php",
      "linkedForm": "formWithHeaderFooter"
    },
    "$_POST": [
      {
        "type": "text",
        "fieldName": "FullName",
        "options": {
          "rules": {
            "core:required": {
              "param": ""
            }
          }
        },
        "name": "FullName"
      },
      {
        "type": "text",
        "fieldName": "Date",
        "options": {
          "rules": {
            "core:required": {
              "param": ""
            }
          }
        },
        "name": "Date"
      },
      {
        "type": "text",
        "fieldName": "FileName",
        "options": {
          "rules": {
            "core:required": {
              "param": ""
            }
          }
        },
        "name": "FileName"
      },
      {
        "type": "text",
        "fieldName": "HeaderFooter",
        "name": "HeaderFooter"
      }
    ]
  },
  "exec": {
    "steps": [
      {
        "name": "folder",
        "module": "core",
        "action": "setvalue",
        "options": {
          "value": "assets/pdf"
        }
      },
      {
        "name": "tmp",
        "module": "core",
        "action": "setvalue",
        "options": {
          "value": "assets/pdf/tmp"
        }
      },
      {
        "name": "",
        "module": "core",
        "action": "condition",
        "options": {
          "if": "{{$_POST.HeaderFooter == '0'}}",
          "then": {
            "steps": [
              {
                "name": "html",
                "module": "core",
                "action": "setvalue",
                "options": {
                  "key": "html",
                  "value": "<div style=\"height:190mm;border: 2px solid red;text-align:center;background:#efefef;\">\n  <h2 style=\"padding-top:25mm\">Certificate of Completion</h2>\n  <p>\n    This is to certify that\n  </p>\n <h1 style=\"color: darkviolet\">\n   {{$_POST.FullName}}\n  </h1>\n  <p>\n    has successfully completed the\n  </p>\n  <p>\n    Wappler Training Program on\n  </p>\n  <p style=\"font-weight:600\">\n    {{$_POST.Date.formatDate('yyyy-MM-dd')}}\n  </p>\n  <p>\n  <img src=\"http://localhost/htmltompdf/assets/images/puppeteer.png\" style=\"height:30px;\"></p>\n</div>"
                }
              },
              {
                "name": "GeneratePDF",
                "module": "HtmlToPDF",
                "action": "ConvertToPDF",
                "options": {
                  "folderName": "{{folder}}",
                  "fileName": "{{$_POST.FileName}}",
                  "tempFolder": "{{tmp}}",
                  "orientation": "L",
                  "pageHeight": 100,
                  "pageWidth": 100,
                  "bodyHTML": "{{html}}",
                  "bodyMarginLeft": 10,
                  "bodyMarginTop": 10,
                  "bodyMarginRight": 10,
                  "bodyMarginBottom": 10,
                  "headerMargin": "",
                  "footerMargin": ""
                },
                "meta": [
                  {
                    "name": "FileName",
                    "type": "text"
                  },
                  {
                    "name": "FilePath",
                    "type": "text"
                  }
                ]
              }
            ]
          },
          "else": {
            "steps": [
              {
                "name": "headerHTML",
                "module": "core",
                "action": "setvalue",
                "options": {
                  "value": "<header style=\"width:100%;text-align: center;\">\n  <p style=\"font-size: 14px;color:#000\">\n    Wappler HTML to PDF Header\n  </p>\n\n</header>"
                }
              },
              {
                "name": "footerHTML",
                "module": "core",
                "action": "setvalue",
                "options": {
                  "value": "<p style=\"width:100%;text-align: center;font-size: 15px;color:#000\">\nUsing PHP mPDF. Page Number (<span >{PAGENO}</span>)\n</p>"
                }
              },
              {
                "name": "html",
                "module": "core",
                "action": "setvalue",
                "options": {
                  "key": "html",
                  "value": "<link rel=\"preconnect\" href=\"https://fonts.gstatic.com\" crossorigin>\n<link href=\"https://fonts.googleapis.com/css2?family=Petit+Formal+Script&display=swap\" rel=\"stylesheet\">\n<div style=\"border: 2px solid red; padding: 30px;text-align:center;background:#efefef\">\n<h2 style=\"font-family: 'Petit Formal Script', cursive;\">Certificate of Completion</h2>\n  <p>\n    This is to certify that\n  </p>\n <h1 style=\"color: magenta\">\n   {{$_POST.FullName}}\n  </h1>\n  <p>\n    has successfully completed the\n  </p>\n  <p>\n    Wappler Training Program on\n  </p>\n  <p style=\"font-weight:600\">\n    {{$_POST.Date.formatDate('yyyy-MM-dd')}}\n  </p>\n  \n</div>"
                }
              },
              {
                "name": "GeneratePDF",
                "module": "HtmlToPDF",
                "action": "ConvertToPDF",
                "options": {
                  "folderName": "{{folder}}",
                  "fileName": "{{$_POST.FileName}}",
                  "tempFolder": "{{tmp}}",
                  "orientation": "L",
                  "pageHeight": 180,
                  "pageWidth": 180,
                  "bodyHTML": "{{html}}",
                  "bodyMarginLeft": 10,
                  "bodyMarginTop": 20,
                  "bodyMarginRight": 10,
                  "bodyMarginBottom": 10,
                  "headerHTML": "{{headerHTML}}",
                  "footerHTML": "{{footerHTML}}",
                  "headerMargin": 10,
                  "footerMargin": 10,
                  "paperSize": "Custom"
                },
                "meta": [
                  {
                    "name": "FileName",
                    "type": "text"
                  },
                  {
                    "name": "FilePath",
                    "type": "text"
                  }
                ]
              }
            ]
          }
        },
        "outputType": "boolean"
      },
      {
        "name": "FilePath",
        "module": "core",
        "action": "setvalue",
        "options": {
          "key": "FilePath",
          "value": "{{GeneratePDF.FilePath}}"
        },
        "output": true
      },
      {
        "name": "FileName",
        "module": "core",
        "action": "setvalue",
        "options": {
          "key": "FileName",
          "value": "{{GeneratePDF.FileName}}"
        },
        "output": true
      }
    ]
  }
}
JSON
);
?>